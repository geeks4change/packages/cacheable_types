<?php
/** @noinspection PhpUnnecessaryStaticReferenceInspection */

namespace Drupal\CacheableTypes\CacheableBool;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableDependencyTrait;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Immutable cacheable boolean with no surprises.
 *
 * It also makes it hard to forget cacheability.
 */
final class CacheableBool extends CacheableValue {

  use CacheableDependencyTrait;

  protected bool $value;

  private function __construct(bool $value, CacheableDependencyInterface $cacheability) {
    $this->value = $value;
    $this->setCacheability($cacheability);
  }

  public static function create(bool $value, CacheableDependencyInterface $cacheability): CacheableBool {
    return new static($value, $cacheability);
  }

  public static function createTrue(): CacheableBool {
    return new static(TRUE, new CacheableMetadata());
  }

  public static function createFalse(): CacheableBool {
    return new static(FALSE, new CacheableMetadata());
  }

  public static function fromAccessResult(AccessResultInterface $accessResult, bool $useNotForbidden = FALSE): CacheableBool {
    $bool = $useNotForbidden ? $accessResult->isAllowed() : !$accessResult->isForbidden();

    $cacheability = $accessResult instanceof CacheableDependencyInterface
      ? $accessResult : NULL;
    return new static($bool, $cacheability);
  }

  public function toAccessResult(bool $useNotForbidden = FALSE): AccessResult {
    $accessResult = $useNotForbidden ? AccessResult::allowedIf($this->value) : AccessResult::forbiddenIf(!$this->value);
    $accessResult->addCacheableDependency($this);
    return $accessResult;
  }

  public static function and(CacheableBool ...$items): CacheableBool {
    foreach ($items as $item) {
      if (!$item->value()) {
        return $item;
      }
    }
    $cacheability = (new CacheableMetadata());
    foreach ($items as $item) {
      $cacheability->addCacheableDependency($item);
    }
    return static::create(TRUE, $cacheability);
  }

  public static function or(CacheableBool ...$items): CacheableBool {
    foreach ($items as $item) {
      if ($item->value()) {
        return $item;
      }
    }
    $cacheability = (new CacheableMetadata());
    foreach ($items as $item) {
      $cacheability->addCacheableDependency($item);
    }
    return static::create(FALSE, $cacheability);
  }

  public static function not(CacheableBool $item): CacheableBool {
    return new static(!$item->value(), $item);
  }

  public function value(): bool {
    return $this->value;
  }

}
