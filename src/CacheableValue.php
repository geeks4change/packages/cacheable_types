<?php

namespace Drupal\CacheableTypes\CacheableBool;

use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Base class for cacheable values.
 */
abstract class CacheableValue implements CacheableDependencyInterface {

  /**
   * @return mixed
   */
  abstract public function value();

}
